package com.example.myapplication.constraintlayout;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.base.BaseActivity;

/**
 * Created by ymz0427 on 2021/10/12
 */
public class ConstraintLayoutActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_constraintlayout1);
    }
}
