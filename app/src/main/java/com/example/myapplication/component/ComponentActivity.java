package com.example.myapplication.component;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.module1.MainModule1Activity;
import com.example.module2.MainModule2Activity;
import com.example.myapplication.base.BaseActivity;
import com.example.myapplication.R;

/**
 * Created by ymz0427 on 2020/12/14
 */
public class ComponentActivity  extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_component);

        TextView textView1 = findViewById(R.id.textView1);
        textView1.setOnClickListener(v -> {
            Intent intent = new Intent(this, MainModule1Activity.class);
            startActivity(intent);
        });

        TextView textView2 = findViewById(R.id.textView2);
        textView2.setOnClickListener(v -> {
            Intent intent = new Intent(this, MainModule2Activity.class);
            startActivity(intent);
        });
    }
}
