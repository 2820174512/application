package com.example.myapplication.utils;

import java.util.Objects;

/**
 * Created by ymz0427 on 2021/10/23
 */
class LifecycleRunnable implements Runnable {

    private Runnable runnable;
    private RunnableObserver observer;

    public LifecycleRunnable(Runnable runnable, RunnableObserver observer) {
        this.runnable = runnable;
        this.observer = observer;
    }

    @Override
    public void run() {
        try {
            if (runnable != null) {
                runnable.run();
            }
        } finally {
            if (observer != null) {
                observer.remove(this);
            }
        }
    }

    public void clean() {
        runnable = null;
        observer = null;
    }
}
