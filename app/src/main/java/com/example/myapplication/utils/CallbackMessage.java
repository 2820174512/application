package com.example.myapplication.utils;

import android.os.Message;

/**
 * Created by ymz0427 on 2021/10/23
 */
class CallbackMessage implements Runnable {

    private final Message message;
    private final Callback callback;

    public CallbackMessage(Message message, Callback callback) {
        this.message = message;
        this.callback = callback;
    }

    @Override
    public void run() {
        try {
            callback.handleMessage(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
