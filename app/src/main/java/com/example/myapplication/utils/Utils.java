package com.example.myapplication.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.util.DisplayMetrics;

public class Utils {


    public static Bitmap loadImage(Resources resources, int id, int width) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, id, options);
        options.inJustDecodeBounds = false;
        options.inDensity = options.outWidth;
        options.inTargetDensity = width;

        return BitmapFactory.decodeResource(resources, id, options);
    }

    private static final Point temp = new Point();

    public static Point getScreenSize(Context context) {
        Resources resources = context.getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();

        temp.x = displayMetrics.widthPixels;
        temp.y = displayMetrics.heightPixels;

        return temp;
    }

    public static boolean equals(double value1, double value2) {
        return equals(value1, value2, 0.0001);
    }

    public static boolean equals(double value1, double value2, double precision) {
        return Math.abs(value1 - value2) < Math.abs(precision);
    }

    public static float range(float value, float max, float min) {
        return Math.min(max, Math.max(value, min));
    }

    public static int range(int value, int max, int min) {
        return Math.min(max, Math.max(value, min));
    }

}
