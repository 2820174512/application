package com.example.myapplication.utils;

import android.os.Message;

/**
 * Created by ymz0427 on 2021/10/23
 */
public interface Callback {
    void handleMessage(Message message);
}
