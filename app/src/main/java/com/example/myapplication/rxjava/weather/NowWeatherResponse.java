package com.example.myapplication.rxjava.weather;

/**
 * Created by ymz0427 on 2021/7/24
 */
public class NowWeatherResponse extends WeatherResponse {

    private NowWeatherModel now;

    public NowWeatherModel getNow() {
        return now;
    }

    public void setNow(NowWeatherModel now) {
        this.now = now;
    }
}
