package com.example.myapplication.rxjava.weather.version1;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ymz0427 on 2021/8/7
 */
public interface QWeatherApi1 {

    @GET("/v7/weather/now")
    Call<ResponseBody> queryNow(@Query("key") String key, @Query("location") String location);
}
