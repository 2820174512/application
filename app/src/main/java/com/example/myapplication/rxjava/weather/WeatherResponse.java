package com.example.myapplication.rxjava.weather;

import java.util.List;

/**
 * Created by ymz0427 on 2021/7/24
 */
public abstract class WeatherResponse {

    //状态码
    private String code;
    //更新时间
    private String updateTime;
    //当前数据的响应式页面
    private String fxLink;

    private Refer refer;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getFxLink() {
        return fxLink;
    }

    public void setFxLink(String fxLink) {
        this.fxLink = fxLink;
    }

    public void setRefer(Refer refer) {
        this.refer = refer;
    }

    public Refer getRefer() {
        return refer;
    }

    public static class Refer {

        private List<String> sources;

        private List<String> license;

        public List<String> getSources() {
            return sources;
        }

        public void setSources(List<String> sources) {
            this.sources = sources;
        }

        public List<String> getLicense() {
            return license;
        }

        public void setLicense(List<String> license) {
            this.license = license;
        }
    }
}
