package com.example.myapplication.rxjava.weather.version4;

import com.example.myapplication.rxjava.weather.NowWeatherResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ymz0427 on 2021/7/24
 */
public interface QWeatherApi4 {

    @GET("/v7/weather/now")
    Observable<NowWeatherResponse> queryNow(@Query(value = "location") String location);

}
