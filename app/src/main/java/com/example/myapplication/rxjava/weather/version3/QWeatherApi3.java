package com.example.myapplication.rxjava.weather.version3;

import com.example.myapplication.rxjava.weather.NowWeatherResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ymz0427 on 2021/8/7
 */
public interface QWeatherApi3 {

    @GET("/v7/weather/now")
    Call<NowWeatherResponse> queryNow(@Query("location") String location);
}
