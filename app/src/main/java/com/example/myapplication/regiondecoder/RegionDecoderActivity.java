package com.example.myapplication.regiondecoder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.myapplication.base.BaseActivity;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ymz0427 on 2021/12/1
 */
public class RegionDecoderActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            InputStream open = getAssets().open("photo.jpg");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bitmap1 = BitmapFactory.decodeStream(open, null, options);
            options.inJustDecodeBounds = false;
            int width = options.outWidth;
            int height = options.outHeight;
            Log.d("BitmapRegionDecoder", "width = " + width + ", height = " + height);


            BitmapRegionDecoder decoder = BitmapRegionDecoder.newInstance(open, true);
            Rect out = new Rect(0, 0, Math.min(width, 100), Math.min(height, 100));
            options.inBitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);

            Bitmap bitmap = decoder.decodeRegion(out, options);
            Log.d("BitmapRegionDecoder", "图片大小：" + bitmap.getAllocationByteCount());

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
