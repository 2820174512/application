package com.example.myapplication.viewpager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ymz0427 on 2021/8/30
 */
public class ViewPagerFragmentAdapter extends FragmentPagerAdapter {

    private final List<Fragment> list = new ArrayList<>();

    public ViewPagerFragmentAdapter(@NonNull FragmentManager fm, List<? extends Fragment> fragments) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        //super(fm);
        if (fragments != null && !fragments.isEmpty()) {
            this.list.addAll(fragments);
        }
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
