package com.example.myapplication.viewpager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.myapplication.nestedscroll.RecyclerViewAdapter;
import com.example.myapplication.nestedscroll.viewpager.InnerRecyclerView;

/**
 * Created by ymz0427 on 2021/8/21
 */
public class RecyclerViewFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        InnerRecyclerView recyclerView = new InnerRecyclerView(requireActivity());
        recyclerView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        RecyclerViewAdapter adapter = new RecyclerViewAdapter("RecyclerView-item", 100);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));
        recyclerView.setAdapter(adapter);

        return recyclerView;
    }
}
