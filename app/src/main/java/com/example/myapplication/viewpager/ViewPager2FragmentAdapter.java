package com.example.myapplication.viewpager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ymz0427 on 2021/8/21
 */
public class ViewPager2FragmentAdapter extends FragmentStateAdapter {



    private final List<Fragment> fragments = new ArrayList<>();

    public ViewPager2FragmentAdapter(FragmentActivity activity, List<Fragment> fragmentList) {
        super(activity);
        if (fragmentList != null) {
            this.fragments.addAll(fragmentList);
        }
    }

    public ViewPager2FragmentAdapter(FragmentActivity activity, int size) {
        super(activity);
        for (int i = 0; i < size; i++) {
            fragments.add(new RecyclerViewFragment());
        }
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return fragments.get(position);
    }

    @Override
    public int getItemCount() {
        return fragments.size();
    }
}
