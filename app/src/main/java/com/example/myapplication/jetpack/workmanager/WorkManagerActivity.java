package com.example.myapplication.jetpack.workmanager;

import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.multiprocess.IListenableWorkerImpl;

import com.example.myapplication.base.BaseActivity;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ymz0427 on 2022/12/8
 */
public class WorkManagerActivity extends BaseActivity {

    public static final String TAG = "worker";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView textView = new TextView(this);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
        textView.setText("启动Work");
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));

        setContentView(textView);

        textView.setOnClickListener(v -> {
            constraintsWork();
        });
    }

    private void work1() {
        Data data = new Data.Builder()
            .putBoolean("test", true).build();

        OneTimeWorkRequest request = new OneTimeWorkRequest.Builder(Worker1.class)
            .setInputData(data)
            .build();

        WorkManager.getInstance(this).getWorkInfoByIdLiveData(request.getId())
                .observe(this, workInfo -> {
                    Log.d(TAG, "当前状态：" + workInfo.getState().name());
                    if (workInfo.getState().isFinished()) {
                        long finishTime = workInfo.getOutputData().getLong("current", 0);
                        Log.d(TAG, String.valueOf(finishTime));
                    }
                });

        WorkManager.getInstance(this).enqueue(request);
    }

    private void multiWork() {

        OneTimeWorkRequest request1 = new OneTimeWorkRequest.Builder(Worker1.class)
            .build();

        OneTimeWorkRequest request2 = new OneTimeWorkRequest.Builder(Worker2.class)
            .build();

        OneTimeWorkRequest request3 = new OneTimeWorkRequest.Builder(Worker3.class)
            .build();

        // worker1、worker2同时执行，执行完再执行worker3
        List<OneTimeWorkRequest> list = Arrays.asList(request1, request2);
        WorkManager.getInstance(this).beginWith(list)
            .then(request3)
            .enqueue();
    }

    private void constraintsWork() {
        Log.d(TAG, "constraintWork started----");
        Constraints constraints = new Constraints.Builder()
            // 联网情况下执行
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build();

        OneTimeWorkRequest request = new OneTimeWorkRequest.Builder(Worker1.class)
            .setConstraints(constraints)
            .build();

        WorkManager.getInstance(this).enqueue(request);
    }



}
