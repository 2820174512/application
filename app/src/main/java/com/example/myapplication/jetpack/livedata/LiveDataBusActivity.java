package com.example.myapplication.jetpack.livedata;

import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;

import com.example.myapplication.base.BaseActivity;

/**
 * Created by ymz0427 on 2022/12/7
 */
public class LiveDataBusActivity extends BaseActivity {

    private static final String TAG = LiveDataBus.TAG;
    private static final String TEST = "test";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView textView = new TextView(this);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
        textView.setText("测试LiveDataBus");
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));

        setContentView(textView);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(new TestAndroidxFragment(), "test");
        fragmentTransaction.commit();

        LiveDataBus.getInstance().hook(false);
        String str = "Hello, World!";
        // 先设置数据，后添加observer，会收到数据，数据回调在onStart执行完后，onResume执行之前
        LiveDataBus.getInstance().with(TEST, String.class).setValue(str);
        Log.d(TAG, "liveData.setValue：" + str);
    }

    @Override
    protected void onStart() {
        LiveDataBus.getInstance()
            .with(TEST, String.class)
            .observe(LiveDataBusActivity.this, new Observer<String>() {
                @Override
                public void onChanged(String string) {
                    Log.d(TAG, this.hashCode() + "--observer before onStart: " + string);
                }
            });
        Log.d(TAG, "before onStart");
        super.onStart();
        Log.d(TAG, "after onStart");
        LiveDataBus.getInstance()
            .with(TEST, String.class)
            .observe(LiveDataBusActivity.this, new Observer<String>() {
                @Override
                public void onChanged(String string) {
                    Log.d(TAG, this.hashCode() + "--observer after onStart: " + string);
                }
            });
    }

    @Override
    protected void onResume() {
        LiveDataBus.getInstance()
            .with(TEST, String.class)
            .observe(LiveDataBusActivity.this, new Observer<String>() {
                @Override
                public void onChanged(String string) {
                    Log.d(TAG, this.hashCode() + "--observer before onResume: " + string);
                }
            });
        Log.d(TAG, "before onResume");
        super.onResume();
        Log.d(TAG, "after onResume");
        LiveDataBus.getInstance()
            .with(TEST, String.class)
            .observe(LiveDataBusActivity.this, new Observer<String>() {
                @Override
                public void onChanged(String string) {
                    Log.d(TAG, this.hashCode() + "--observer after onResume: " + string);
                }
            });
    }
}
