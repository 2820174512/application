package com.example.myapplication.jetpack.databinding;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;

import com.example.myapplication.R;
import com.example.myapplication.base.BaseActivity;
import com.example.myapplication.databinding.LayoutDataBindingActivityBinding;

/**
 * Created by ymz0427 on 2022/12/7
 * 数据绑定见：https://developer.android.google.cn/topic/libraries/data-binding
 */
public class DataBindingActivity extends BaseActivity {

    private static final String TAG = "DataBindingActivity";

    private final ObservableField<String> name = new ObservableField<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutDataBindingActivityBinding binding =
            DataBindingUtil.setContentView(this, R.layout.layout_data_binding_activity);

        User user = new User();
        user.setName("Admin");
        user.setPassword("123456");

        binding.setUser(user);
        // 单向绑定： @{user.name}
        // 双向绑定   @={user.name}

        binding.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DataBindingActivity.this, user.toString(), Toast.LENGTH_LONG).show();
                Log.d(TAG, user.toString());

                binding.name.setText("Hello");
            }
        });

    }
}
