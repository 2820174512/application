package com.example.myapplication.jetpack.livedata;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;

/**
 * Created by ymz0427 on 2022/12/23
 */
public class TestAndroidxFragment extends Fragment {

    @Override
    public void onStart() {
        super.onStart();
        Log.d(LiveDataBus.TAG, "TestAndroidXFragment-onStart");

        getLifecycle().addObserver(new LifecycleEventObserver() {
            @Override
            public void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {
                Log.d(LiveDataBus.TAG, "TestAndroidXFragment-" + event);
            }
        });
    }
}
