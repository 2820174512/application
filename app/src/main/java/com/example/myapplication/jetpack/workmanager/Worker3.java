package com.example.myapplication.jetpack.workmanager;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

/**
 * Created by ymz0427 on 2022/12/9
 */
public class Worker3 extends Worker {


    public Worker3(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        Log.d(WorkManagerActivity.TAG, "worker3 started---");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(WorkManagerActivity.TAG, "worker3 ended---");

        return Result.success();
    }
}
