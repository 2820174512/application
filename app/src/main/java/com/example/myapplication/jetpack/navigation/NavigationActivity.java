package com.example.myapplication.jetpack.navigation;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.myapplication.R;
import com.example.myapplication.base.BaseActivity;
import com.example.myapplication.databinding.LayoutNavigationBinding;

/**
 * Created by ymz0427 on 2022/12/9
 */
public class NavigationActivity extends BaseActivity {

    private DrawerLayout drawerLayout;
    private AppBarConfiguration appBarConfiguration;
    private NavController navController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com.example.myapplication.databinding.LayoutNavigationBinding binding = DataBindingUtil.setContentView(this,
            R.layout.layout_navigation);

        this.drawerLayout = binding.drawerLayout;
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_fragment);

        this.navController = navHostFragment.getNavController();
        this.appBarConfiguration = new AppBarConfiguration.Builder(R.id.fragment1, R.id.fragment2)
            .setOpenableLayout(drawerLayout)
            .build();

        setSupportActionBar(binding.toolbar);


        NavigationUI.setupActionBarWithNavController(this, this.navController, this.appBarConfiguration);

        NavigationUI.setupWithNavController(binding.navigationView, this.navController);
    }

    // 在标题是 左上角菜单 2
    // 点击 标题上 左上角菜单 触发的返回动作  [点击事件]
    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, appBarConfiguration) || super.onSupportNavigateUp();
    }
}
