package com.example.myapplication.jetpack.workmanager;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

/**
 * Created by ymz0427 on 2022/12/9
 */
public class Worker1 extends Worker {

    private final WorkerParameters workerParameters;

    public Worker1(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        workerParameters = workerParams;
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.d(WorkManagerActivity.TAG, "worker1 started---");
        boolean test = workerParameters.getInputData().getBoolean("test", false);
        Log.d(WorkManagerActivity.TAG, "test = " + test);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(WorkManagerActivity.TAG, "worker1 ended---");

        Data out = new Data.Builder()
            .putLong("current", System.currentTimeMillis())
            .build();

        return Result.success(out);
    }
}
