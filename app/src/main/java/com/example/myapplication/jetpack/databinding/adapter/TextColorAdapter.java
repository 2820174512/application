package com.example.myapplication.jetpack.databinding.adapter;

import android.util.Log;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;

/**
 * Created by ymz0427 on 2022/12/7
 */
public class TextColorAdapter {

    public static final String TAG = "TextColorAdapter";

    // BindingAdapter处理自定义属性， 见TextViewBindingAdapter
    @BindingAdapter({"textColor"})
    public static void setTextColor(MyTextView textView, int color) {
        textView.setTextColor(color);
        Log.d(TAG, "setTextColor");
    }

    @InverseBindingAdapter(attribute = "textColor", event = "android:textColorAttrChanged")
    public static int getColor(MyTextView textView) {
        int color = textView.getCurrentTextColor();
        Log.d(TAG, "getColor:" + color);
        return color;
    }

    @BindingAdapter(value = "android:textColorAttrChanged", requireAll = false)
    public static void setTextColorListener(MyTextView textView, InverseBindingListener textColorAttrChanged) {
        Log.d(TAG, "setTextColorListener");
        textView.setListener(new MyTextView.TextColorListener() {
            @Override
            public void onTextColor(int color) {
                if (textColorAttrChanged != null) {
                    textColorAttrChanged.onChange();
                }
            }
        });
    }
}
