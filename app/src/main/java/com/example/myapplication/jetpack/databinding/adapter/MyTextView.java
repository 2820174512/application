package com.example.myapplication.jetpack.databinding.adapter;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by ymz0427 on 2022/12/7
 */
public class MyTextView extends AppCompatTextView {

    private TextColorListener listener;

    public MyTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setTextColor(int color) {
        super.setTextColor(color);
        if (listener != null) {
            listener.onTextColor(color);
        }
    }

    public void setListener(TextColorListener listener) {
        this.listener = listener;
    }

    interface TextColorListener {
        void onTextColor(int color);
    }
}
