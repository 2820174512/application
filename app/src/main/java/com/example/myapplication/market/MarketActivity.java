package com.example.myapplication.market;


import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;


import com.example.myapplication.base.BaseActivity;
import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MarketActivity extends BaseActivity {

    //https://www.jianshu.com/p/cfb7f212a5a2
    private static final List<String> marketList = Arrays.asList(
            "com.xiaomi.market",
            "com.huawei.appmarket"
    );


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_market);


        TextView text = findViewById(R.id.text);
        TextView xiaomi = findViewById(R.id.xiaomi);
        TextView huawei = findViewById(R.id.huawei);

        text.setOnClickListener(v -> {
            try {
                Uri uri = Uri.parse("market://details?id=" + "windinfo.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                List<ResolveInfo> list = getBaseContext().getPackageManager().queryIntentActivities(intent, PackageManager.GET_RESOLVED_FILTER);
                if (list.isEmpty()) {
                    Toast.makeText(MarketActivity.this, "您的手机没有安装Android应用市场", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(intent);
                }

            } catch (Exception e) {
                Toast.makeText(MarketActivity.this, "您的手机没有安装Android应用市场", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });


        List<String> list = getInstallAppMarkets(this);
        xiaomi.setOnClickListener(v -> {
            if (!list.contains(marketList.get(0))) {
                launchAppDetail(this, "windinfo.android", marketList.get(0));
            } else {
                Toast.makeText(MarketActivity.this, "您的手机没有安装小米应用市场", Toast.LENGTH_SHORT).show();
            }
        });

        huawei.setOnClickListener(view -> {
            if (!list.contains(marketList.get(1))) {
                launchAppDetail(this, "windinfo.android", marketList.get(1));
            } else {
                Toast.makeText(MarketActivity.this, "您的手机没有安装华为应用市场", Toast.LENGTH_SHORT).show();
            }
        });


    }


    //    https://www.jianshu.com/p/dc5f40b5466f
    public static ArrayList<String> getInstallAppMarkets(Context context) {
        //默认的应用市场列表，有些应用市场没有设置APP_MARKET通过隐式搜索不到
        ArrayList<String> pkgList = new ArrayList<>(marketList);

        ArrayList<String> pkgs = new ArrayList<>();
        if (context == null)
            return pkgs;
        Intent intent = new Intent();
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.APP_MARKET");
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> infos = pm.queryIntentActivities(intent, 0);
        if (infos.size() == 0)
            return pkgs;
        int size = infos.size();
        for (int i = 0; i < size; i++) {
            String pkgName = "";
            try {
                ActivityInfo activityInfo = infos.get(i).activityInfo;
                pkgName = activityInfo.packageName;

            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!TextUtils.isEmpty(pkgName))
                pkgs.add(pkgName);

        }
        //取两个list并集,去除重复
        pkgList.removeAll(pkgs);
        pkgs.addAll(pkgList);
        return pkgs;
    }

    public static void launchAppDetail(Context context, String appPkg, String marketPkg) {
        try {
            if (TextUtils.isEmpty(appPkg))
                return;
            Uri uri = Uri.parse("market://details?id=" + appPkg);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (!TextUtils.isEmpty(marketPkg))
                intent.setPackage(marketPkg);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
