package com.example.myapplication.viewgroup;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.example.myapplication.base.BaseActivity;
import com.example.myapplication.R;

import java.util.Arrays;
import java.util.List;


/**
 * Created by ymz0427 on 2021/7/31
 */
public class FlowActivity extends BaseActivity {

    private final List<String> words = Arrays.asList("家用电器", "手机", "运营商", "数码", "电脑",
            "办公", "电子书", "惠普星系列高清一体机", "格力2匹移动空调", "彩色喷墨打印机", "茶水杯", "1T SSD固态硬盘");

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_flow);

        FlowLayout layout = findViewById(R.id.flow_layout);


        for (int i = 0; i < words.size(); i++) {
            TextView textView = new TextView(this);
            textView.setText(words.get(i));
            textView.setBackground(ContextCompat.getDrawable(this, R.drawable.round_background));
            textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, FlowLayout.dp2px(60)));
            //textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            int padding = FlowLayout.dp2px(5);
            textView.setPadding(padding, padding, padding, padding);

            layout.addView(textView);
        }
    }
}
