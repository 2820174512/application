package com.example.myapplication;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.base.BaseRecyclerAdapter;

/**
 * Created by ymz0427 on 2021/8/22
 */
public class StringAdapter extends BaseRecyclerAdapter<String, StringAdapter.StringViewHolder> {

    private final Context context;

    private OnItemClickListener onItemClickListener;

    public StringAdapter(Context context) {
        this.context = context;
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public StringViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView textView = new TextView(context);
        textView.setGravity(Gravity.CENTER);
        textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        return new StringViewHolder(textView);
    }

    @Override
    public void onBindViewHolder(@NonNull final StringViewHolder holder, final int position) {
        holder.textView.setText(getItem(position));
        holder.textView.setPadding(0, 30, 0, 30);
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.OnItemClick(holder.getAdapterPosition(), holder.textView.getText().toString());
                }
            }
        });
    }

    public static class StringViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        private StringViewHolder(View itemView) {
            super(itemView);
            if (itemView instanceof TextView) {
                textView = (TextView) itemView;
            }
        }
    }

    public interface OnItemClickListener {
        void OnItemClick(int position, String str);
    }
}
