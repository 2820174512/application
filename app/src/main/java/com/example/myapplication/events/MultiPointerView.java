package com.example.myapplication.events;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.utils.Utils;

/**
 * 多指屏幕触控
 */
public class MultiPointerView extends View {

    private Bitmap bitmap;
    private Paint paint;

    private float offsetX;
    private float offsetY;

    private float downX;
    private float downY;

    private float lastOffsetX;
    private float lastOffsetY;

    private int currentPointerId;

    public MultiPointerView(Context context) {
        super(context);
        init();
    }

    public MultiPointerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MultiPointerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        bitmap = Utils.loadImage(getResources(), R.drawable.photo, (int) (Utils.getScreenSize(getContext()).x * 0.75));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.translate(offsetX, offsetY);
        canvas.drawBitmap(bitmap, 0, 0, paint);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:

            case MotionEvent.ACTION_POINTER_DOWN:
                //如果有新按下的手指，则只有该手指可以滑动
                currentPointerId = event.getPointerId(event.getActionIndex());
                downX = event.getX(event.getActionIndex());
                downY = event.getY(event.getActionIndex());
                lastOffsetX = offsetX;
                lastOffsetY = offsetY;
                break;

            case MotionEvent.ACTION_MOVE:
                int activePointerIndex = event.findPointerIndex(currentPointerId);
                offsetX = lastOffsetX + event.getX(activePointerIndex) - downX;
                offsetY = lastOffsetY + event.getY(activePointerIndex) - downY;

                invalidate();
                break;

            case MotionEvent.ACTION_POINTER_UP:
                int pointerIndex = event.getActionIndex();
                int pointerId = event.getPointerId(pointerIndex);
                if (pointerId == currentPointerId) {
                    int index;
                    if (pointerIndex == event.getPointerCount() - 1) {
                        //抬起的手最后一根手指
                        index = event.getPointerCount() - 2;
                    } else {
                        index = event.getPointerCount() - 1;
                    }
                    currentPointerId = event.getPointerId(index);
                    downX = event.getX(index);
                    downY = event.getY(index);
                    lastOffsetX = offsetX;
                    lastOffsetY = offsetY;
                }

                break;

            default:
                break;

        }


        return true;
    }
}
