package com.example.myapplication.events;

/**
 * Created by ymz0427 on 2021/9/18
 */
public interface IRecycle {
    void recycle();
}
