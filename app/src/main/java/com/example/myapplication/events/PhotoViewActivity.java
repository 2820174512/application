package com.example.myapplication.events;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.myapplication.base.BaseActivity;

/**
 * Created by ymz0427 on 2021/9/16
 */
public class PhotoViewActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MultiPointerView view = new MultiPointerView(this);
        setContentView(view);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
