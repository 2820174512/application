package com.example.myapplication.deeplink;

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.TextView
import com.example.myapplication.base.BaseActivity

/**
 * Created by ymz0427 on 2022/11/28
 *
 */
class TestDeeplinkActivity : BaseActivity() {

    private val url = "https://www.baidu.com?key1=value1&key2=value2"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(TextView(this).apply {
            gravity = Gravity.CENTER
            text = buildString {
                append("跳转deeplink， 参数target=test, url: ")
                append(url)
            }
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW, Uri.Builder()
                    .scheme("myscheme")
                    .encodedAuthority("myhost:2222")
                    .path("/path1")
                    .appendQueryParameter("target", "test")
                    .appendQueryParameter("url", url)
                    .build())
                intent.data = Uri.parse("myscheme://myhost:2222/path1?target=test&url=https%3A%2F%2Fwww.baidu.com%3Fkey1%3Dvalue1%26key2%3Dvalue2")

                startActivity(intent)
            }
        })
    }
}