package com.example.myapplication.deeplink;

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TextView
import com.example.base.JsonUtils
import com.example.myapplication.base.BaseActivity

/**
 * Created by ymz0427 on 2022/11/28
 *
 */
class DeeplinkActivity : BaseActivity() {

    companion object {
        private const val TAG = "deeplink"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(TextView(this).apply {
            gravity = Gravity.CENTER
            layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT)
            text = "Hello, Deeplink"
        })
        printData("onCreate", intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        printData("onNewIntent", intent)
    }

    private fun printData(tag : String, intent: Intent?) {
        val data = intent?.data
        val map = mutableMapOf<String, String>()
        data?.queryParameterNames?.forEach {
            data.getQueryParameter(it)?.apply {
                map[it] = this
            }
        }
        Log.d("$TAG-$tag", JsonUtils.toJson(map))
    }
}