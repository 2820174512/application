package com.example.myapplication.intent;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.base.BaseActivity;

/**
 * Created by ymz0427 on 2022/10/7
 */
public class IntentActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_intent);

        findViewById(R.id.singleTask).setOnClickListener(v -> {
            Intent intent = new Intent(IntentActivity.this, SingleTaskActivity.class);
            startActivity(intent);
        });
    }
}
