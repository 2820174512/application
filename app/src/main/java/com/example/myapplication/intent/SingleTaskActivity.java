package com.example.myapplication.intent;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.myapplication.base.BaseActivity;

/**
 * Created by ymz0427 on 2022/10/7
 */
public class SingleTaskActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
