package com.example.myapplication.memory;

import android.app.ActivityManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.myapplication.base.BaseActivity;

/**
 * Created by ymz0427 on 2022/1/5
 */
public class MemoryActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            ActivityManager activityManager = getSystemService(ActivityManager.class);
            Log.d("Test", "最大内存： " + activityManager.getMemoryClass() + "M");
            Log.d("Test", "largeHeap = true时最大内存： " + activityManager.getLargeMemoryClass() + "M");
        }
    }
}
