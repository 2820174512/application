package com.example.myapplication.custom;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.myapplication.base.BaseActivity;

/**
 * Created by ymz0427 on 2022/12/24
 */
public class MyGlideActivity extends BaseActivity {

    String url = "https://images.pexels.com/photos/464359/pexels-photo-464359.jpeg?cs=srgb&dl=pexels-francesco-ungaro-464359.jpg&fm=jpg";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageView imageView = new ImageView(this);

        Glide.with(this).load(url)
            .fitCenter()
            .into(imageView);
    }
}
