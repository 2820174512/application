package com.example.myapplication.jni;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.myapplication.base.BaseActivity;

/**
 * Created by ymz0427 on 2023/2/16
 */
public class JNIActivity extends BaseActivity {

    static {
        System.loadLibrary("myapplication");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(this, getJNIString(), Toast.LENGTH_LONG).show();
    }

    public native String getJNIString();
}
