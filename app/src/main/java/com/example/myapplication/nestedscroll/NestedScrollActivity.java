package com.example.myapplication.nestedscroll;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.example.myapplication.base.BaseActivity;
import com.example.myapplication.R;
import com.example.myapplication.viewpager.ViewPager2FragmentAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

/**
 * Created by ymz0427 on 2021/8/21
 */
public class NestedScrollActivity extends BaseActivity {


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_nest_scroll);

        RecyclerView topView = findViewById(R.id.topView);
        topView.setNestedScrollingEnabled(false);
        TabLayout tabLayout = findViewById(R.id.tablayout);
        ViewPager2 viewPager2 = findViewById(R.id.viewpager2);

        topView.setLayoutManager(new LinearLayoutManager(this));
        topView.setAdapter(new RecyclerViewAdapter("TopView-item", 9));

        ViewPager2FragmentAdapter adapter = new ViewPager2FragmentAdapter(this, 4);
        viewPager2.setAdapter(adapter);

        new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(String.format("第%s个", position));
            }
        }).attach();
    }
}
