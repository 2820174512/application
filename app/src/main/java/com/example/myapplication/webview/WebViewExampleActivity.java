package com.example.myapplication.webview;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.base.autoservice.ServiceLoaderUtils;
import com.example.common.autoservice.IWebViewService;
import com.example.myapplication.base.BaseActivity;

/**
 * Created by ymz0427 on 2021/9/29
 */
public class WebViewExampleActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView textView = new TextView(this);
        textView.setGravity(Gravity.CENTER);
        textView.setText("跳转到WebView");

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IWebViewService service = ServiceLoaderUtils.getService(IWebViewService.class);
                if (service != null) {
                    service.startWebView(WebViewExampleActivity.this, service.getAbsolutePath("demo.html"),
                            null, true);
//                    service.startWebView(WebViewExampleActivity.this, "https://www.baidu.com1",
//                            null, true);
                }
            }
        });
        setContentView(textView);
    }
}
