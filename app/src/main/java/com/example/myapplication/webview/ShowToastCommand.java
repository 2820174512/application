package com.example.myapplication.webview;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.text.TextUtils;
import android.widget.Toast;

import com.example.myapplication.base.MainApplication;
import com.example.webview.webviewprocess.command.Command;
import com.google.auto.service.AutoService;


/**
 * Created by ymz0427 on 2021/10/2
 */
@AutoService({Command.class})
public class ShowToastCommand implements Command {

    @Override
    public String getName() {
        return "showToast";
    }

    @Override
    public void execute(String params) {
        System.out.println("执行进程id：" + Process.myPid() + ", 参数：" + params);
        if (!TextUtils.isEmpty(params)) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainApplication.context, params, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
