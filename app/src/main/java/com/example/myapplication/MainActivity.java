package com.example.myapplication;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.anr.ANRActivity;
import com.example.myapplication.autosize.AutoSizeActivity;
import com.example.myapplication.base.BaseActivity;
import com.example.myapplication.component.ComponentActivity;
import com.example.myapplication.constraintlayout.ConstraintLayoutActivity;
import com.example.myapplication.coordinatorlayout.CoordinatorLayoutMainActivity;
import com.example.myapplication.deeplink.TestDeeplinkActivity;
import com.example.myapplication.events.PhotoViewActivity;
import com.example.myapplication.custom.MyGlideActivity;
import com.example.myapplication.handler.HandlerActivity;
import com.example.myapplication.intent.IntentActivity;
import com.example.myapplication.jetpack.databinding.DataBindingActivity;
import com.example.myapplication.jetpack.databinding.adapter.DataBindingAdapterActivity;
import com.example.myapplication.jetpack.livedata.LiveDataBusActivity;
import com.example.myapplication.jetpack.livedata.LiveDataBusHookActivity;
import com.example.myapplication.jetpack.navigation.NavigationActivity;
import com.example.myapplication.jetpack.workmanager.WorkManagerActivity;
import com.example.myapplication.jni.JNIActivity;
import com.example.myapplication.market.MarketActivity;
import com.example.myapplication.memory.MemoryActivity;
import com.example.myapplication.nestedscroll.NestedScrollActivity;
import com.example.myapplication.regiondecoder.RegionDecoderActivity;
import com.example.myapplication.rxjava.RxJavaActivity;
import com.example.myapplication.threadcheck.ViewActivity;
import com.example.myapplication.types.TypeTestActivity;
import com.example.myapplication.viewgroup.FlowActivity;
import com.example.myapplication.webview.WebViewExampleActivity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class MainActivity extends BaseActivity {

    protected Map<String, Class<? extends BaseActivity>> getMap() {
        return new LinkedHashMap<String, Class<? extends BaseActivity>>() {{
            put("应用市场", MarketActivity.class);
            put("ANR测试", ANRActivity.class);
            put("泛型类型获取", TypeTestActivity.class);
            put("invalidate异常分析", ViewActivity.class);
            put("组件化", ComponentActivity.class);
            put("使用retrofit、rxjava、gson处理http请求", RxJavaActivity.class);
            put("流式布局", FlowActivity.class);
            put("滑动冲突", NestedScrollActivity.class);
            put("事件分发", PhotoViewActivity.class);
            put("CoordinatorLayout", CoordinatorLayoutMainActivity.class);
            put("WebView", WebViewExampleActivity.class);
            put("约束布局", ConstraintLayoutActivity.class);
            put("Handler", HandlerActivity.class);
            put("大图片加载", RegionDecoderActivity.class);
            put("内存测试", MemoryActivity.class);
            put("intent测试", IntentActivity.class);
            put("自适应字体大小", AutoSizeActivity.class);
            put("deeplink", TestDeeplinkActivity.class);
            put("livedatabus", LiveDataBusActivity.class);
            put("livedatabusex", LiveDataBusHookActivity.class);
            put("数据绑定", DataBindingActivity.class);
            put("绑定适配器", DataBindingAdapterActivity.class);
            put("work manager", WorkManagerActivity.class);
            put("navigation", NavigationActivity.class);
            put("MyGlide", MyGlideActivity.class);
            put("jni", JNIActivity.class);
        }};
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        Map<String, Class<? extends BaseActivity>> map = getMap();

        StringAdapter adapter = new StringAdapter(this);
        adapter.setData(new ArrayList<>(map.keySet()));
        recyclerView.setAdapter(adapter);

        setContentView(recyclerView);

        DividerItemDecoration decoration = new DividerItemDecoration(this, RecyclerView.VERTICAL);
        decoration.setDrawable(new ColorDrawable(Color.parseColor("#000000")) {
            @Override
            public int getIntrinsicHeight() {
                return 1;
            }
        });
        recyclerView.addItemDecoration(decoration);

        adapter.setOnItemClickListener(new StringAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int position, String str) {
                Intent intent = new Intent(MainActivity.this, map.get(str));
                startActivity(intent);
            }
        });
    }



}
