package com.example.myapplication.coordinatorlayout.example2;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;

import com.example.myapplication.utils.Utils;

/**
 * Created by ymz0427 on 2021/9/24
 */
public class HeaderBehavior extends CoordinatorLayout.Behavior<View> {

    private int initTop;
    private int currentOffsetY;

    public HeaderBehavior() {
    }

    public HeaderBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onLayoutChild(@NonNull CoordinatorLayout parent, @NonNull View child, int layoutDirection) {
        parent.onLayoutChild(child, layoutDirection);
        initTop = child.getTop();
        return true;
    }

    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View child,
                                       @NonNull View directTargetChild, @NonNull View target, int axes, int type) {
        return true;
    }

    @Override
    public void onNestedPreScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View child,
                                  @NonNull View target, int dx, int dy, @NonNull int[] consumed, int type) {
        if (dy > 0) {
            scrollChildY(child, dy, consumed);
        }
    }

    @Override
    public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View child,
                               @NonNull View target, int dxConsumed, int dyConsumed,
                               int dxUnconsumed, int dyUnconsumed, int type, @NonNull int[] consumed) {
        if (dyUnconsumed < 0) {
            scrollChildY(child, dyUnconsumed, consumed);
        }
    }

    private void scrollChildY(View child, int dy, int[] consumed) {
        int offsetY = currentOffsetY - dy;
        offsetY = Utils.range(offsetY, 0, -child.getHeight());
        ViewCompat.offsetTopAndBottom(child, offsetY - (child.getTop() - initTop));
        consumed[1] = currentOffsetY - offsetY;
        currentOffsetY = offsetY;
    }
}
