package com.example.myapplication.coordinatorlayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.base.BaseActivity;
import com.example.myapplication.coordinatorlayout.example1.CoordinatorLayout1Activity;
import com.example.myapplication.coordinatorlayout.example2.CoordinatorLayout2Activity;

/**
 * Created by ymz0427 on 2021/9/21
 */
public class CoordinatorLayoutMainActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_coordinatorlayout_main);

        TextView textView1 = findViewById(R.id.textView1);
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CoordinatorLayoutMainActivity.this, CoordinatorLayout1Activity.class);
                startActivity(intent);
            }
        });

        TextView textView2 = findViewById(R.id.textView2);
        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CoordinatorLayoutMainActivity.this, CoordinatorLayout2Activity.class);
                startActivity(intent);
            }
        });
    }
}
