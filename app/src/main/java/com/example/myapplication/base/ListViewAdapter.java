package com.example.myapplication.base;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.base.BaseRecyclerAdapter;

import java.util.List;

/**
 * Created by ymz0427 on 2021/8/22
 */
public class ListViewAdapter<T, VH extends RecyclerView.ViewHolder> extends BaseAdapter {

    private final BaseRecyclerAdapter<T, VH> adapter;


    public ListViewAdapter(BaseRecyclerAdapter<T, VH> adapter) {
        this.adapter = adapter;
    }

    public void setData(List<T> list) {
        this.adapter.setData(list);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return adapter.getItemCount();
    }

    @Override
    public T getItem(int position) {
        return adapter.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return adapter.getItemViewType(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VH vh;
        if (convertView == null) {
            vh = adapter.createViewHolder(parent, getItemViewType(position));
            vh.itemView.setTag(vh);
        } else {
            try {
                vh = (VH) convertView.getTag();
            } catch (Exception e) {
                vh = adapter.createViewHolder(parent, getItemViewType(position));
            }
        }
        adapter.bindViewHolder(vh, position);

        return vh.itemView;
    }
}
