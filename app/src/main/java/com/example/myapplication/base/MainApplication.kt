package com.example.myapplication.base

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.alibaba.android.arouter.launcher.ARouter
import com.example.base.BaseApplication
import com.example.base.loadsir.EmptyCallback
import com.example.base.loadsir.ErrorCallback
import com.example.base.loadsir.LoadingCallback
import com.example.base.loadsir.TimeoutCallback
import com.kingja.loadsir.core.LoadSir

class MainApplication : BaseApplication() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var context : Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        ARouter.init(this)

        LoadSir.beginBuilder()
                .addCallback(ErrorCallback())
                .addCallback(LoadingCallback())
                .addCallback(TimeoutCallback())
                .addCallback(EmptyCallback())
                .setDefaultCallback(LoadingCallback::class.java)
                .commit();
    }
}