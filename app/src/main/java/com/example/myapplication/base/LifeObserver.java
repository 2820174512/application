package com.example.myapplication.base;



import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;


public class LifeObserver implements LifecycleObserver {

    private String tag = null;

    public LifeObserver(Object object) {
        if (object != null) {
            tag = object.getClass().getSimpleName();
        }
    }

    private static final String TAG = "LifeObserver";

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void onCreate() {
        Log.d(tag == null ? TAG : tag, "onCreate");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onResume() {
        Log.d(tag == null ? TAG : tag, "onResume");
    }
}
