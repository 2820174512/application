package com.example.myapplication.base;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ymz0427 on 2021/8/22
 */
public abstract class BaseRecyclerAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {


    private final List<T> list = new ArrayList<>();


    public void setData(List<T> list) {
        this.list.clear();
        if (list != null && !list.isEmpty()) {
            this.list.addAll(list);
        }
        Collections.reverse(this.list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public T getItem(int position) {
        return list.get(position);
    }
}
