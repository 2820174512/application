package com.example.myapplication.autosize;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.TypedValue;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.base.BaseActivity;

/**
 * Created by ymz0427 on 2022/10/8
 */
public class AutoSizeActivity extends BaseActivity {

    private static final int START = 1;
    private static final int END = 100;

    private final Handler handler = new Handler(Looper.getMainLooper(), this::handleMessage);



    private TextView textView;
    private int size;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_autosize);

        textView = findViewById(R.id.textView);
        size = START;
        handler.sendEmptyMessage(0);
    }

    private boolean handleMessage(Message message) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        if (size >= END) {
            return false;
        }
        size++;
        handler.sendEmptyMessageDelayed(0, 100);

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
}
