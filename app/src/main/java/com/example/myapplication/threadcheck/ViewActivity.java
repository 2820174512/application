package com.example.myapplication.threadcheck;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.myapplication.base.BaseActivity;

public class ViewActivity extends BaseActivity {

    private static final String TAG = "ViewActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView textView = new TextView(this);
        textView.setText("Hello, World!");
        setContentView(textView);

        textView.setOnClickListener(this::onClick);
    }

    private void onClick(View view) {
        if (!(view instanceof TextView)) {
            return;
        }
        TextView textView = (TextView) view;
        new Thread(() -> {
            textView.invalidate();
        }).start();
    }
}
