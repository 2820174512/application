#include <jni.h>
#include <string>
#include <android/log.h>

extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_myapplication_jni_JNIActivity_getJNIString(JNIEnv *env, jobject thiz) {
    jstring pJstring = env->NewStringUTF("Hello world from jni!");
    return pJstring;
}