package com.example.module1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;

@Route(path = "/module1/MainModule1Activity", group = "module1")
public class MainModule1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_module1);

        TextView textView = findViewById(R.id.textView);
        textView.setOnClickListener(v -> {
            ARouter.getInstance().build("/module2/MainModule2Activity")
                    .withString("from", "" + getClass().getName())
                    .navigation(this);
        });
    }
}