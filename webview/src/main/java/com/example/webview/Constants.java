package com.example.webview;

/**
 * Created by ymz0427 on 2021/9/28
 */
public class Constants {

    public static final String TITLE = "title";

    public static final String URL = "url";

    public static final String SHOW_ACTION_BAR = "show_action_bar";

    public static final String ENABLE_REFRESH = "enable_refresh";

    public static final String ANDROID_ASSETS = "file:///android_asset/";
}
