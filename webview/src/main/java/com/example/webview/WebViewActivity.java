package com.example.webview;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

/**
 * Created by ymz0427 on 2021/9/28
 */
public class WebViewActivity extends AppCompatActivity {

    private TextView title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_webview);

        title = findViewById(R.id.title);
        title.setText(getIntent().getStringExtra(Constants.TITLE));

        View navigation = findViewById(R.id.navigation);
        boolean showNavigation = getIntent().getBooleanExtra(Constants.SHOW_ACTION_BAR, true);
        if (showNavigation) {
            navigation.setVisibility(View.VISIBLE);
        } else {
            navigation.setVisibility(View.GONE);
        }

        View back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String url = getIntent().getStringExtra(Constants.URL);
        WebViewFragment webViewFragment = WebViewFragment.newInstance(url, true);

        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = supportFragmentManager.beginTransaction();
        transaction.replace(R.id.container, webViewFragment).commit();
    }

    public void updateTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            this.title.setText(title);
        }
    }
}
