package com.example.webview;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.base.loadsir.ErrorCallback;
import com.example.base.loadsir.LoadingCallback;
import com.example.webview.webviewprocess.CustomWebView;
import com.kingja.loadsir.callback.Callback;
import com.kingja.loadsir.core.LoadService;
import com.kingja.loadsir.core.LoadSir;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

/**
 * Created by ymz0427 on 2021/9/28
 */
public class WebViewFragment extends Fragment implements WebViewCallback, OnRefreshListener {


    public static WebViewFragment newInstance(String url, boolean enableRefresh) {

        Bundle bundle = new Bundle();
        bundle.putString(Constants.URL, url);
        bundle.putBoolean(Constants.ENABLE_REFRESH, enableRefresh);

        WebViewFragment fragment = new WebViewFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private String url;
    private boolean enableRefresh = true;

    private boolean isError;

    private LoadService<?> register;

    private SmartRefreshLayout layout;

    private CustomWebView webView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            url = bundle.getString(Constants.URL);
            enableRefresh = bundle.getBoolean(Constants.ENABLE_REFRESH);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = new SmartRefreshLayout(inflater.getContext());
        layout.setEnableLoadMore(false);
        layout.setEnableRefresh(enableRefresh);
        layout.setOnRefreshListener(this);

        webView = new CustomWebView(inflater.getContext());
        webView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


        layout.addView(webView);
        register = LoadSir.getDefault().register(layout, new Callback.OnReloadListener() {
            @Override
            public void onReload(View v) {
                register.showCallback(LoadingCallback.class);
                webView.reload();
            }
        });

        webView.registerCallback(this);
        if (!TextUtils.isEmpty(url)) {
            webView.loadUrl(url);
        }

        return register.getLoadLayout();
    }

    @Override
    public void pageStart(String url) {
        isError = false;
        if (register != null) {
            register.showCallback(LoadingCallback.class);
        }
    }

    @Override
    public void pageFinish(String url) {
        if (isError) {
            if (register != null) {
                register.showCallback(ErrorCallback.class);
            }
            layout.setEnableRefresh(true);
        } else {
            if (register != null) {
                register.showSuccess();
            }
            layout.setEnableRefresh(enableRefresh);
        }
        layout.finishRefresh();
    }

    @Override
    public void pageError() {
        isError = true;
        layout.finishRefresh();
    }

    @Override
    public void updateTitle(String title) {
        Activity activity = getActivity();
        if (activity instanceof WebViewActivity) {
            ((WebViewActivity) activity).updateTitle(title);
        }
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        webView.reload();
    }
}
