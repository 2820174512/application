package com.example.webview.bean;

import com.google.gson.JsonObject;

/**
 * Created by ymz0427 on 2021/10/1
 */
public class JsCommand {

    private String name;

    private JsonObject param;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JsonObject getParam() {
        return param;
    }

    public void setParam(JsonObject param) {
        this.param = param;
    }
}
