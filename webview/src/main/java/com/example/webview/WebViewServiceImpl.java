package com.example.webview;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.common.autoservice.IWebViewService;
import com.google.auto.service.AutoService;

/**
 * Created by ymz0427 on 2021/9/28
 */
@AutoService({IWebViewService.class})
public class WebViewServiceImpl implements IWebViewService {

    @Override
    public void startWebView(Context context, String url, String title, boolean showActionBar) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(Constants.TITLE, title);
        intent.putExtra(Constants.URL, url);
        intent.putExtra(Constants.SHOW_ACTION_BAR, showActionBar);
        context.startActivity(intent);
    }

    @Override
    public Fragment getWebFragment(String url, boolean enableRefresh) {
        return WebViewFragment.newInstance(url, enableRefresh);
    }

    @Override
    public String getAbsolutePath(@NonNull String html) {
        if (html.startsWith("/")) {
            return Constants.ANDROID_ASSETS + html.substring(1);
        } else {
            return Constants.ANDROID_ASSETS + html;
        }
    }


}
