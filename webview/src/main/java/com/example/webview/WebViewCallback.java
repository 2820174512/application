package com.example.webview;

/**
 * Created by ymz0427 on 2021/9/30
 */
public interface WebViewCallback {
    void pageStart(String url);
    void pageFinish(String url);
    void pageError();

    void updateTitle(String title);
}
