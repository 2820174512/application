package com.example.webview.webviewprocess.dispatcher;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;

import com.example.base.BaseApplication;
import com.example.webview.IMainProcessToWebViewProcessInterface;
import com.example.webview.IWebViewProcessToMainProcessInterface;
import com.example.webview.webviewprocess.CustomWebView;
import com.example.webview.webviewprocess.command.Command;
import com.example.webview.mainprocess.MainProcessService;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * Created by ymz0427 on 2021/10/2
 */
public class WebViewCommandDispatcher implements ServiceConnection {

    private static WebViewCommandDispatcher dispatcher;

    public static WebViewCommandDispatcher getInstance() {
        if (dispatcher == null) {
            synchronized (WebViewCommandDispatcher.class) {
                if (dispatcher == null) {
                    dispatcher = new WebViewCommandDispatcher();
                }
            }
        }
        return dispatcher;
    }

    private IWebViewProcessToMainProcessInterface service;


    private WebViewCommandDispatcher() {

    }



    public void initProcessService() {
        Intent intent = new Intent(BaseApplication.getInstance(), MainProcessService.class);
        BaseApplication.getInstance().bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    public void executeCommand(String commandName, String params, CustomWebView webView) {
        if (service != null) {
            try {
                if (params == null) {
                    throw new RemoteException("参数不能为null");
                }
                if (params.contains("callbackname")) {
                    service.handleCommandWithCallback(commandName, params, new IMainProcessToWebViewProcessInterface.Stub() {
                        @Override
                        public void onResponse(String callbackname, String response) throws RemoteException {
                            System.out.println("返回进程：" + Process.myPid());
                            webView.handleCallback(callbackname, response);
                        }
                    });
                } else {
                    service.handleCommand(commandName, params);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        this.service = IWebViewProcessToMainProcessInterface.Stub.asInterface(service);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        this.service = null;
        initProcessService();
    }

    @Override
    public void onBindingDied(ComponentName name) {
        this.service = null;
        initProcessService();
    }
}
