package com.example.webview.webviewprocess.webchromeclient;

import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.example.webview.WebViewCallback;

/**
 * Created by ymz0427 on 2021/9/30
 */
public class CustomWebChromeClient extends WebChromeClient {

    private static final String TAG = "CustomWebChromeClient";

    private final WebViewCallback callback;

    public CustomWebChromeClient(WebViewCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onReceivedTitle(WebView view, String title) {
        super.onReceivedTitle(view, title);
        if (callback != null) {
            callback.updateTitle(title);
        }
    }

    @Override
    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        Log.d(TAG, consoleMessage.message());
        return super.onConsoleMessage(consoleMessage);
    }
}
