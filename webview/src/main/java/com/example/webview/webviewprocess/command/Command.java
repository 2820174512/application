package com.example.webview.webviewprocess.command;

import com.example.webview.IMainProcessToWebViewProcessInterface;

/**
 * Created by ymz0427 on 2021/10/2
 */
public interface Command {

    String getName();

    void execute(String params);

    default void execute(String params, IMainProcessToWebViewProcessInterface callback) {
        execute(params);
    }
}
