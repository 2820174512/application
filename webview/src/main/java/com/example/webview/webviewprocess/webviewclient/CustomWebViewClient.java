package com.example.webview.webviewprocess.webviewclient;

import android.graphics.Bitmap;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.webview.WebViewCallback;

/**
 * Created by ymz0427 on 2021/9/30
 */
public class CustomWebViewClient extends WebViewClient {


    private final WebViewCallback callback;

    public CustomWebViewClient(WebViewCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        if (callback != null) {
            callback.pageStart(url);
        }
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (callback != null) {
            callback.pageFinish(url);
        }
    }

    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        super.onReceivedError(view, request, error);
        if (callback != null) {
            callback.pageError();
        }
    }
}
