// IMainProcessToWebViewProcessInterface.aidl
package com.example.webview;

// Declare any non-default types here with import statements

interface IMainProcessToWebViewProcessInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void onResponse(String callbackname, String response);
}