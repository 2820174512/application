// IWebViewProcessToMainProcessInterface.aidl
package com.example.webview;

// Declare any non-default types here with import statements

import com.example.webview.IMainProcessToWebViewProcessInterface;

interface IWebViewProcessToMainProcessInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void handleCommand(String command, String params);

    void handleCommandWithCallback(String command, String params, IMainProcessToWebViewProcessInterface callback);
}