package com.example.base;

import android.app.Application;

/**
 * Created by ymz0427 on 2021/9/29
 */
public class BaseApplication extends Application {

    private static BaseApplication application;

    public static BaseApplication getInstance() {
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
    }
}
