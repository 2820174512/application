package com.example.base;

import com.google.gson.Gson

/**
 * Created by ymz0427 on 2022/11/28
 *
 */
object JsonUtils {


    fun <T> toJson(obj : T) = Gson().toJson(obj)
}