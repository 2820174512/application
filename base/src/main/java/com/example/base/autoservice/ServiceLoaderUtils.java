package com.example.base.autoservice;

import java.util.ServiceLoader;

/**
 * Created by ymz0427 on 2021/9/29
 */
public class ServiceLoaderUtils {

    public static <T> T getService(Class<T> clazz) {
        ServiceLoader<T> load = ServiceLoader.load(clazz);
        if (load != null) {
            for (T t : load) {
                return t;
            }
        }
        return null;
    }
}
