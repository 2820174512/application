package com.example.common.autoservice;


import android.content.Context;

import androidx.fragment.app.Fragment;

/**
 * Created by ymz0427 on 2021/9/28
 */
public interface IWebViewService {

    void startWebView(Context context, String url, String title, boolean showActionBar);

    Fragment getWebFragment(String url, boolean enableRefresh);

    String getAbsolutePath(String html);
}
