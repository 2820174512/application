package com.example.module2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Route;

@Route(path = "/module2/MainModule2Activity", group = "module2")
public class MainModule2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_module2);

        String from = getIntent().getStringExtra("from");
        Toast.makeText(this, String.valueOf(from), Toast.LENGTH_SHORT).show();
    }
}